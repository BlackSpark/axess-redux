<?php
/**
 * Created by PhpStorm.
 * User: jonak
 * Date: 12/18/2017
 * Time: 3:14 PM
 */

/**
 * Contains programmatic interaction with an SQLite3 Database for the purposes of course information.
 *
 * Schema based on what's available via the Stanford ExploreCourses API at:
 *     http://explorecourses.stanford.edu/search?view=xml-20140630&q=COURSE_QUERY_HERE
 */
class CourseDB extends SQLite3
{
    /** @var SQLite3Stmt Parameterized query for adding a course. */
    private $addCourseQuery;

    /** @var SQLite3Stmt Parameterized query for adding a department. */
    private $addDepartmentQuery;

    /** @var SQLite3Stmt Parameterized query for adding a section. */
    private $addSectionQuery;

    /** @var SQLite3Stmt Parameterized query for adding a schedule. */
    private $addScheduleQuery;

    /** @var SQLite3Stmt Parameterized query for adding a learning objective. */
    private $addLearningObjectiveQuery;

    /** @var SQLite3Stmt Parameterized query for adding a GER. */
    private $addGerQuery;

    // TODO: Queries for getting info from the database

    function __construct() {
        $this->open("test.sqlite");
        $this->exec('PRAGMA FOREIGN_KEYS = ON');

        $this->exec('CREATE TABLE IF NOT EXISTS departments (
    code                  TEXT        NOT NULL PRIMARY KEY UNIQUE,
    name                  TEXT        NOT NULL
);'
        );

        $this->exec('CREATE TABLE IF NOT EXISTS courses (
    id                    INTEGER     NOT NULL PRIMARY KEY ASC AUTOINCREMENT UNIQUE,
    department            TEXT,
    courseCode             TEXT,
    title                 TEXT,
    description           TEXT,
    repeatable            BOOLEAN,
    letterGrading         BOOLEAN,
    creditNoCreditGrading BOOLEAN,
    minimumUnits          INTEGER,
    maximumUnits          INTEGER     NOT NULL,
    
    FOREIGN KEY(department) REFERENCES departments(code)
);'
        );

        $this->exec('CREATE TABLE IF NOT EXISTS sections (
    id                    INTEGER     NOT NULL PRIMARY KEY ASC AUTOINCREMENT UNIQUE,
    relatedCourse          INTEGER     NOT NULL,
    term                  TEXT,
    termId                INTEGER,
    department            TEXT,
    code                  TEXT,
    units                 INTEGER,
    sectionNumber         INTEGER,
    component             TEXT,
    numEnrolled           INTEGER,
    maxEnrolled           INTEGER,
    numWaitlist           INTEGER,
    maxWaitlist           INTEGER,
    enrollStatus          TEXT,
    addConsentRequired    BOOLEAN,
    dropConsentRequired   BOOLEAN,
    
    FOREIGN KEY(relatedCourse) REFERENCES courses(id),
    FOREIGN KEY(department) REFERENCES departments(code)
);'
        );

        $this->exec('CREATE TABLE IF NOT EXISTS schedules (
    courseId              INTEGER     NOT NULL PRIMARY KEY,
    sectionId             INTEGER,
    startDate             DATE,
    startTime             TIME,
    endDate               DATE,
    endTime               TIME,
    
    FOREIGN KEY(courseId) REFERENCES courses(id),
    FOREIGN KEY(sectionId) REFERENCES sections(id)
);'
        );

        $this->exec('CREATE TABLE IF NOT EXISTS learningObjectives (
    id                    INTEGER     NOT NULL PRIMARY KEY,
    code                  TEXT        NOT NULL,
    description           TEXT,
    
    FOREIGN KEY(id) REFERENCES courses(id)
);'
        );

        $this->exec('CREATE TABLE IF NOT EXISTS gers (
    id                    INTEGER     NOT NULL PRIMARY KEY,
    code                  TEXT        NOT NULL,
    description           TEXT,
    
    FOREIGN KEY(id) REFERENCES courses(id)
);'
        );

        $this->addDepartmentQuery = $this->prepare('INSERT INTO departments (code, name) VALUES (?, ?)');
        $this->addCourseQuery = $this->prepare('INSERT INTO courses (id, department, courseCode, title, description, repeatable, letterGrading, creditNoCreditGrading, minimumUnits, maximumUnits) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
        $this->addSectionQuery = $this->prepare('INSERT INTO sections (id, relatedCourse, term, termId, department, code, units, sectionNumber, component, numEnrolled, maxEnrolled, numWaitlist, maxWaitlist, enrollStatus, addConsentRequired, dropConsentRequired) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
        $this->addScheduleQuery = $this->prepare('INSERT INTO schedules (courseId, sectionId, startDate, startTime, endDate, endTime) VALUES (?, ?, ?, ?, ?, ?)');
        $this->addLearningObjectiveQuery = $this->prepare('INSERT INTO learningObjectives (id, code, description) VALUES (?, ?, ?)');
        $this->addGerQuery = $this->prepare('INSERT INTO gers (id, code, description) VALUES (?, ?, ?)');
    }

    public function addDepartment(string $code, string $name) {
        if(is_null($code)) {
            throw new InvalidArgumentException("\$code cannot be null!");
        }

        $this->addDepartmentQuery->bindValue(1, $code);
        $this->addDepartmentQuery->bindValue(2, $name);
        $retval = $this->addDepartmentQuery->execute();
        if($retval == false) {
            return false;
        }
        $retval->finalize();
        return true;
    }

    //TODO: add* functions
    //TODO: get* functions

}